import java.awt.geom.Rectangle2D;

public class Comuna {
    private Individuo person;
    private Rectangle2D territory; // Alternatively: double width, length;

    //Constructors
    public Comuna(){
        territory = new Rectangle2D.Double(0, 0, 1000, 1000); // 1000x1000 m²;
    }
    public Comuna(double width, double length){
        territory = new Rectangle2D.Double(0,0, width, length);
        person = null;
    }

    //Getters
    public double getWidth() {
        return territory.getWidth();
    }   //Retorna el ancho del territorio de la comuna (i.e. rango eje x).
    public double getHeight() {
        return territory.getHeight();
    } //Retorna el largo del territorio de la comuna (i.e. rango eje y).
    public String getState(){
        return (person.getState());
    }//Actualiza la comuna, en este caso solo la persona se ve afectada, y en base al state calulado anteriormente, se actualiza sus nuevas coordenadas.
    public static String getStateDescription(){
        return("coord-x, " + "coord-y");
    } //En esta Stage lo unico que necesitamos es la ubicacion de la persona que se esta moviendo cada cierto tiempo.

    //Set Methods
    public void setPerson(Individuo person){
        this.person = person;
        double rx = Math.random()*getWidth();
        double ry = Math.random()*getHeight();
        //llamar x, y en person y usarlos
        person.updateState(rx, ry);
    } //Asigna la persona a la comuna y la coloca en una coordenada x-y aleatoria dentro de los rangos permitidos.
    public void updateState() {
        person.updateState(person.getX_tPlusDelta(), person.getY_tPlusDelta());
    }

    //State Methods
    public void computeNextState (double delta_t) {
        person.computeNextState(delta_t);
    } //Calcula la futura posicion de la persona despues de delta_t [s]  transcurridos.
 }
