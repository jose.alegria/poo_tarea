import java.io.PrintStream;

public class Simulador {
    private Comuna comuna;
    private PrintStream out;

    private Simulador() {
    }

    public Simulador(PrintStream output, Comuna comuna) {
        out = output;
        this.comuna = comuna;
    }

    private void printStateDescription() {
        ;
        String s = "time,\t" + Comuna.getStateDescription();
        out.println(s);
    }

    private void printState(double t) {
        java.lang.String s = t + ",\t";
        s += comuna.getState();
        out.println(s);
    }

    /**
     * @param delta_t      time step
     * @param endTime      simulation time
     * @param samplingTime time between printing states to not use delta_t that would generate too many lines.
     * @param distanciaInf minimum distance for an infection to be possible between people.
     * @param prob0        probability of infection when both people interacting have no face masks.
     * @param tiempoInfeccion  time for individual to recover from covid after they got infected.
     * @param prob1 probability of infection when only one person out of the two interacting, wears a face mask.
     * @param prob2 probability of infection when both people interacting are wearing face masks.
     * @param VacTime time for Vacunatorios to be "actived" for "S" people to get vaccinated.
     * @param cant number of Vacunatorios to be "actived" in la "Comuna"
     * @param size length of one the sides of each Vacunatorio in Comuna.
     */
    public void simulate(double delta_t, double endTime, double samplingTime, double distanciaInf, double prob0,
            double tiempoInfeccion, double prob1, double prob2, double VacTime, int cant, double size) { // simulate
                                                                                                         // time passing
        double t = 0;
        double cont = 0;
        printStateDescription();
        printState(t);
        while (t < endTime) {
            for (double nextStop = t + samplingTime; t < nextStop; t += delta_t) {
                comuna.computeNextState(delta_t, distanciaInf, prob0, tiempoInfeccion, prob1, prob2); // compute its next state based on current global state.
                comuna.updateState(); // update its state
            }
            if ((t >= VacTime) && (cont == 0)) {
                comuna.setVac(cant, size);
                cont++;
            } //When simulation time reaches VacTime, vacunatorios appear somewhere in the Comuna's territory.
            printState(t);
        }
    }
}
