import java.util.ArrayList;
import java.awt.geom.Point2D;

public class Individuo {
    private double x, y, speed, angle, deltaAngle;
    private Point2D ubicacion;
    private double x_tPlusDelta, y_tPlusDelta;
    private Comuna comuna;
    private String estadoSalud;
    private String estadoSalud_tPlusDelta;
    private double timerRecuperacion;
    private String FaceMask;

    // Constructor
    public Individuo(Comuna comuna, double speed, double deltaAngle, String salud) {
        x = 0;
        y = 0;
        angle = Math.random() * 2 * Math.PI; // Le asigna un angulo de direccion aleatorio a la persona entre 0-2PI rad.
        this.comuna = comuna;
        this.speed = speed;
        this.deltaAngle = deltaAngle;
        this.estadoSalud = salud;
        this.estadoSalud_tPlusDelta = salud;
        timerRecuperacion = 0;
        this.FaceMask = "OFF";
        ubicacion = new Point2D.Double(x,y);
    }

    // Get Methods
    public double getX() {
        return x;
    }
    public double getY() {
        return y;
    }
    public double getX_tPlusDelta() {
        return x_tPlusDelta;
    }
    public double getY_tPlusDelta() {
        return y_tPlusDelta;
    }
    public static String getStateDescription() {
        return "Estado de Salud";
    }
    public String getState() {
        return estadoSalud;
    }
    public String getMascara() {
        return FaceMask;
    }

    // State Methods
    public void LookforPersonInf(ArrayList<Individuo> close_person, double d, double p0, double p1, double p2) {
        for (int i = 0; i < close_person.size(); i++) {
            double D = Math.sqrt((x - (close_person.get(i).getX())) * (x - (close_person.get(i).getX()))
                    + (y - (close_person.get(i).getY())) * (y - (close_person.get(i).getY())));
            if (D <= d && estadoSalud == "S" && close_person.get(i).getState() == "I") {
                if (FaceMask == "ON" && close_person.get(i).getMascara() == "ON") {
                    double r = Math.random();
                    if (r <= p2) {
                        estadoSalud_tPlusDelta = "I";
                    }
                } else if ((FaceMask == "ON" && close_person.get(i).getMascara() == "OFF")
                        || (FaceMask == "OFF" && close_person.get(i).getMascara() == "ON")) {
                    double r = Math.random();
                    if (r <= p1) {
                        estadoSalud_tPlusDelta = "I";
                    }
                } else if (FaceMask == "OFF" && close_person.get(i).getMascara() == "OFF") {
                    double r = Math.random();
                    if (r <= p0) {
                        estadoSalud_tPlusDelta = "I";
                    }
                }
            }
        } // Calcula la distancia entre el individuo y cada uno de los otros que se
          // encuentran en la comuna, y una vez que encuentra
          // uno lo suficientemente cerca, revisa segun las probabilidades si es que se
          // infectara.
    }// Metodo utilizado para escanear las personas cerca del individuo.
    public void LookForVacuna() {
        for (int i = 0; i < comuna.getVacunatorios().size(); i++) {
            if (comuna.getVacunatorios().get(i).getVacunatory().contains(ubicacion.getX(), ubicacion.getY())
                    && estadoSalud == "S") {
                estadoSalud_tPlusDelta = "V";
            } //Llamamos cada vacunatorio del array de la comuna, para revisar a traves del metodo contain(x,y), para ver si la persona se encuentra dentro de un vacunatorio.
        }
    } //Revisa si se encontrara pasando por un vacunatorio en su proximo estado, si es asi se recuperara.
    public void startTimer(double delta_t, double tiempoInfeccion) {
        timerRecuperacion += delta_t;
        if (timerRecuperacion >= tiempoInfeccion) {
            estadoSalud_tPlusDelta = "R";
            timerRecuperacion = 0;
        }
    } //Este metodo que solo se ejecuta lo que esta adentro, si el individuo se infecta (estadoSalud == "I")
    public void computeAngle(){
        double r = Math.random(); // r entre 0 y 1
        double r2 = Math.random(); // r entre 0 y 1 para ver naturaleza del cambio de angulo
        if (r2 >= 0.5) {
            angle += r * deltaAngle; // cambia el angulo con delta positivo
        } else {
            angle -= r * deltaAngle; // cambia el angulo con delta negativo
        }
    } //Calculo del prox angulo en delta_t
    public void computeXY(double delta_t){
        double time;
        double time2;
        double angle2 = angle;

        if ((x + speed * delta_t * Math.cos(angle)) < 0) {
            time = x / (speed * (Math.abs(Math.cos(angle))));
            time2 = delta_t - time;
            x_tPlusDelta = x + speed * time * (Math.cos(angle));
            angle = (3 * Math.PI) - angle;
            x_tPlusDelta = x_tPlusDelta + speed * time2 * (Math.cos(angle));
        } else if ((x + speed * delta_t * Math.cos(angle)) > comuna.getWidth()) {
            time = (comuna.getWidth() - x) / (speed * (Math.abs(Math.cos(angle))));
            time2 = delta_t - time;
            x_tPlusDelta = x + speed * time * (Math.cos(angle));
            angle = (3 * Math.PI) - angle;
            x_tPlusDelta = x_tPlusDelta + speed * time2 * (Math.cos(angle));
        } else {
            x_tPlusDelta = x + speed * delta_t * Math.cos(angle);
        }

        if ((y + speed * delta_t * Math.sin(angle2)) < comuna.getHeight()
                && (y + speed * delta_t * Math.sin(angle2)) > 0) {
            y_tPlusDelta = y + speed * delta_t * Math.sin(angle2);

        } else if (y + speed * delta_t * Math.sin(angle2) > comuna.getHeight()) {

            time = (comuna.getHeight() - y) / (speed * (Math.abs(Math.sin(angle2))));
            time2 = delta_t - time;
            y_tPlusDelta = y + speed * time * Math.sin(angle2);
            angle2 = (2 * Math.PI) - angle2;
            y_tPlusDelta = y_tPlusDelta + (speed * time2 * (Math.sin(angle2)));
            angle = angle2;

        } else if (y + speed * delta_t * Math.sin(angle2) < 0) {
            time = y / (speed * (Math.abs(Math.sin(angle2))));
            time2 = delta_t - time;
            y_tPlusDelta = y + speed * time * Math.sin(angle2);
            angle2 = (2 * Math.PI) - angle2;
            y_tPlusDelta = y_tPlusDelta + (speed * time2 * ((Math.sin(angle2))));
            angle = angle2;
        }
    } //Calculo de x_tPlusDelta y y_tPlusDelta
    public void computeNextState(double delta_t, double d, double p0, double tiempoInfeccion, double p1, double p2) {
        if (estadoSalud == "S") {
            LookForVacuna();
        } // Solo si es susceptible revisara por vacunatorios, ya que son los unicos que se pueden vacunar para pasar a "V"
        if (estadoSalud == "S" && estadoSalud_tPlusDelta != "V") {
            LookforPersonInf(comuna.getPeople(), d, p0, p1, p2); //Solo busca por infectados si es "S" y si no fue vacunado.
        } else if (estadoSalud == "I") {
            startTimer(delta_t, tiempoInfeccion); //Y si es infectado su timer personal sigue actualizandose hasta que se recupere, para pasar a "R".
        }
        computeAngle(); // aumenta x e y tomando el angulo nuevo y el tiempo de mov.
        computeXY(delta_t); // si la persona sale de la comuna rebota
    } //Se preocupa de actulizar variasbles como estadoSalud_tPlusDelta, x_tPlusdeta y y_tPlusdeta.

    //Set Methods
    public void setMask() {
        FaceMask = "ON";
    }

    //Update Methods
    public void updateState() {
        x = x_tPlusDelta;
        y = y_tPlusDelta;
        ubicacion.setLocation(x_tPlusDelta, y_tPlusDelta);
        estadoSalud = estadoSalud_tPlusDelta;
    } //Va actualizando posiciones y estados de salud del individuo.
    public void updateStateRandom(double x, double y) {
        this.x = x;
        this.y = y;
        estadoSalud = estadoSalud_tPlusDelta;
    } //Solo es utilizado cuando la comuna busca colocar al individuo en un lugar random de su territorio.
}
