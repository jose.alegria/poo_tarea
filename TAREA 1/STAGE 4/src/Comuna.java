import java.awt.geom.Rectangle2D;
import java.util.ArrayList;

public class Comuna {
    private ArrayList<Individuo> people;
    private Rectangle2D territory; // Alternatively: double width, length;
    private ArrayList<Vacunatorio> vacunatorios;
    private double I = 0, R = 0, S = 0, V = 0;

    // Constructors
    public Comuna() {
        territory = new Rectangle2D.Double(0, 1000, 1000, 1000); // 1000x1000 m²;
    }
    public Comuna(double width, double length) {
        territory = new Rectangle2D.Double(0, length, width, length);
        people = new ArrayList<Individuo>();
        vacunatorios = new ArrayList<Vacunatorio>();
    }

    // Getters
    public double getWidth() {
        return territory.getWidth();
    } // Retorna el ancho del territorio de la comuna (i.e. rango eje x).
    public double getHeight() {
        return territory.getHeight();
    } // Retorna el largo del territorio de la comuna (i.e. rango eje y).
    public ArrayList<Individuo> getPeople() {
        return people;
    }
    public ArrayList<Vacunatorio> getVacunatorios(){
        return vacunatorios;
    }
    public static String getStateDescription() {
        return ("Vac, " + "Inf, " + "Rec, " + "Sus");
    } // En esta Stage lo que necesitamos reportar es el numero de Infectados, Recuperados y Susceptibles dentro de la comuna.
    public String getState() {
        I = 0;
        S = 0;
        R = 0;
        V = 0; // Se crean contadores para luego mostrar por pantalla cantidad de Inf, Rec,
        // Sus.
        int N = people.size();
        for (int i = 0; i < N; i++) {
            if (people.get(i).getState() == "I") {
                I += 1;
            } else if ((people.get(i).getState()) == "S") {
                S += 1;
            } else if (people.get(i).getState() == "R") {
                R += 1;
            } else if (people.get(i).getState() == "V") {
                V += 1;
            }

        }
        return (V + ", " + I + ", " + R + ", " + S);
    } // Retorna cantidad de infectados, recuperados y susceptibles en la comuna.

    //Set Methods
    public void setPerson(Individuo person) {
        people.add(person);
        double rx = Math.random() * getWidth();
        double ry = Math.random() * getHeight();
        person.updateStateRandom(rx, ry);
    } // Asigna la persona a la comuna y la coloca en una coordenada x-y aleatoria dentro de los rangos permitidos.
    public void setMaskPeople(double M) {
        int cant = (int) (people.size() * M);
        for (int i = 0; i < cant; i++) {
            int rand = (int) (Math.random() * people.size());
            if (people.get(rand).getMascara() == "ON") {
                i--;
            } else {
                people.get(rand).setMask();
            }
        }
    } // Se asignan mascarillas de forma aleatoria a la gente de la comuna.
    public void setVac(int cant, double size) {
        for (int i = 0; i < cant; i++) {
            Vacunatorio vacun = new Vacunatorio(size, this);
            vacunatorios.add(vacun);
        }

    } //Asigna vacunatoris al array de la comuna, y los coloca en una ubicacion random dentro de su territorio.

    //State Methods
    public void computeNextState(double delta_t, double d, double p0, double tiempoInfeccion, double p1, double p2) {

        for (int i = 0; i < people.size(); i++) {
            people.get(i).computeNextState(delta_t, d, p0, tiempoInfeccion, p1, p2);
        }
    } // Calcula la futura posicion de la personas despues de delta_t [s], tambien se encarga de calcular una posible infeccion.
    public void updateState() {
        for (int i = 0; i < people.size(); i++) {
            (people.get(i)).updateState();
        }
    } // Actualiza la comuna, las personas con sus nuevas posiciones en x,y. Tambien
      // de sus nuevos estados de salud depediendo de cuanto tiempo han pasado al lado
      // de "I" persona.
}
