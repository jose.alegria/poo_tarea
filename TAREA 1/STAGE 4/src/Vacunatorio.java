import java.awt.geom.Rectangle2D;

public class Vacunatorio {
    private Rectangle2D AreaVac;
    private Comuna comuna;
    //Creamos este objeto el cual es un cuadrado llamado vacunatorio, por donde la gente
    // con estado de salud S, puede vacunarse pasando a "V" trabaja en conjunto a Point2D de java.
    public Vacunatorio() {
        AreaVac = new Rectangle2D.Double(0, 10, 10, 10);
    }

    public Vacunatorio(double size, Comuna lugar) {
        comuna = lugar;
        double coord_x, coord_y;
        while (true) {
            coord_x = comuna.getWidth() * Math.random();
            if (coord_x < comuna.getWidth() - size) {
                break;
            }
        }
        while (true) {
            coord_y = comuna.getHeight() * Math.random();
            if (coord_y > size) {
                break;
            }
        } //Al momento de iniciarlizar cada vacunatorio nos aseguramos de que no pudieran salirse de los bordes del territorio.
        AreaVac = new Rectangle2D.Double(coord_x, coord_y, size, size);
    }
    public Rectangle2D getVacunatory(){
        return AreaVac;
    } //Retorna el area que aborda el vacunatorio en un sistema x-y
}
