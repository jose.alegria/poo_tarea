Este proyecto busca proyectar el comportamiento de una pandemia altamente contagiosa en un espacio confinado con múltiples variables a considerar. Los archivos que la componen son:

- makefile.txt
- configurationFile.txt
- individuo.java
- Comuna.java
- Simulacion.java
- Stage4.java
- Vacunatorio.java

**Instrucciones de compilación:**
Primero descargar archivos de cada Stage por separado, asegurarse de que se mantenga cada stage en su respectivo directorio para evitar confusión de archivos de nombres similares.
Una vez que cada carpeta esté descargada, continuar con la ejecución etapa por etapa, para esto le damos uso al archivo makefile.
Estando parado en la carpeta del stage que se desea compilar, se utilizan los siguientes comandos (en Linux):
1. make clean (elimina archivos .class)
2. make (genera archivos .class)
3. make run (ejecuta el codigo)

Durante la realización de la tarea decidimos no optar por el crédito extra ofrecido.
