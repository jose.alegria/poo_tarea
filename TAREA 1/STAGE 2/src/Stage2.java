import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class Stage2 {
    public static void main(String [] args) throws IOException {
        if (args.length != 1) {
            System.out.println("Usage: java Stage1Main <configurationFile.txt>");
            System.exit(-1);
        }
        Scanner s=new Scanner(new File(args[0]));
        System.out.println("File: "+args[0]);
        double simulationDuration = s.nextDouble();
        System.out.println("Simulation time: "+simulationDuration);
        double individuosIniciales = s.nextDouble();
        double individuosInfectados = s.nextDouble();
        double tiempoInfeccion = s.nextDouble();
        s.nextLine();
        double comunaWidth = s.nextDouble();
        double comunaLength = s.nextDouble();
        s.nextLine();
        double speed = s.nextDouble();
        double delta_t = s.nextDouble();
        double deltaAngle = s.nextDouble();
        s.nextLine();
        double distanciaContagio = s.nextDouble();
        double probabilidadContagio = s.nextDouble();
        double samplingTime = 1.0;  // 1 [s]
        Comuna comuna = new Comuna(comunaWidth, comunaLength);
        for(int i=0; i<individuosIniciales;i++) {
            if (i<individuosInfectados){
                Individuo person = new Individuo(comuna, speed, deltaAngle, "I");
                comuna.setPerson(person);
                System.out.println(person.getX() + ", "+ person.getY());
            }else {
                Individuo person = new Individuo(comuna, speed, deltaAngle, "S");
                comuna.setPerson(person);
                System.out.println(person.getX() + ", "+ person.getY());
            } // Se ocupa un for para crear individuos N o I, y se van asignando al array de la comuna
        }
        Simulador sim = new Simulador(System.out, comuna);
        sim.simulate(delta_t, simulationDuration,samplingTime,distanciaContagio,probabilidadContagio,tiempoInfeccion);
    }
}
