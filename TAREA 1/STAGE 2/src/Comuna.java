import java.awt.geom.Rectangle2D;
import java.util.ArrayList;

public class Comuna {
    private ArrayList<Individuo> people;
    private Rectangle2D territory; // Alternatively: double width, length;
    private double I=0, R=0, S=0;

    //Constructors
    public Comuna(){
        territory = new Rectangle2D.Double(0, 0, 1000, 1000); // 1000x1000 m²;
    }
    public Comuna(double width, double length){
            territory = new Rectangle2D.Double(0,0, width, length);
            people = new ArrayList<Individuo>();
    }

    //Getters
    public double getWidth() {
        return territory.getWidth();
    }   //Retorna el ancho del territorio de la comuna (i.e. rango eje x).
    public double getHeight() {
        return territory.getHeight();
    } //Retorna el largo del territorio de la comuna (i.e. rango eje y).
    public ArrayList<Individuo> getPeople(){return people;} //Retorna el grupo de personas que son de la comuna.
    public static String getStateDescription(){
        return("Inf, " + "Rec, " + "Sus");
    } //En esta Stage lo que necesitamos reportar es el numero de Infectados, Recuperados y Susceptibles dentro de la comuna.
    public String getState(){
        I = 0;
        S = 0;
        R = 0; //Se crean contadores para luego mostrar por pantalla cantidad de Inf, Rec, Sus.
        int N = people.size();
        for (int i=0; i< N; i++){
            if (people.get(i).getState() == "I"){
                I+=1;
            }else if((people.get(i).getState()) == "S"){
                S+=1;
            }else if(people.get(i).getState() == "R"){
                R+=1;
            }
        }
        return(I + ", " + R + ", " + S);
    } //Retorna cantidad de infectados, recuperados y susceptibles en la comuna.

    //Set Methods
    public void setPerson(Individuo person){
        people.add(person);
        double rx = Math.random()*getWidth();
        double ry = Math.random()*getHeight();
        person.updateStateRandom(rx, ry);
    } //Asigna la persona a la comuna y la coloca en una coordenada x-y aleatoria dentro de los rangos permitidos.

    //State methods
    public void computeNextState (double delta_t, double d, double p0, double tiempoInfeccion) {
        for(int i=0; i<people.size();i++ ){
            people.get(i).computeNextState(delta_t, d, p0, tiempoInfeccion);
        }
    } //Calcula la futura posicion de la personas despues de delta_t [s], tambien se encarga de calcular una posible infeccion.
    public void updateState(){
        for(int i=0; i< people.size(); i++){
            (people.get(i)).updateState();
        }
    } //Actualiza la comuna, las personas con sus nuevas posiciones en x,y. Tambien de sus nuevos estados de salud depediendo de cuanto tiempo han pasado al lado de "I" persona.
 }
