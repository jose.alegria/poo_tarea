import java.util.ArrayList;


public class Individuo {
    private double x, y, speed, angle, deltaAngle;
    private double x_tPlusDelta, y_tPlusDelta;
    private Comuna comuna;
    private String estadoSalud;
    private String estadoSalud_tPlusDelta;
    private double timerRecuperacion;
    private String FaceMask;

    // Constructor
    public Individuo(Comuna comuna, double speed, double deltaAngle, String salud) {
        x = 0;
        y = 0;
        angle = Math.random() * 2 * Math.PI; // Le asigna un angulo de direccion aleatorio a la persona entre 0-2PI rad.
        this.comuna = comuna;
        this.speed = speed;
        this.deltaAngle = deltaAngle;
        this.estadoSalud = salud;
        this.estadoSalud_tPlusDelta = salud;
        timerRecuperacion = 0;
        this.FaceMask = "OFF";
    }

    // Get Methods
    public double getX() {
        return x;
    } //Retorna coord-x  del individuo.
    public double getY() {
        return y;
    } //Retorna coord-y del individuo.
    public double getX_tPlusDelta() {
        return x_tPlusDelta;
    } //Retorna futura coord-x del individuo, que es calculada en computeNextState.
    public double getY_tPlusDelta() {
        return y_tPlusDelta;
    }//Retorna futura coord-y del individuo, que es calculada en computeNextState.
    public static String getStateDescription() {
        return "Estado de Salud";
    }
    public String getState() {
        return estadoSalud;
    } //Retorna el estado de salud de la persona.
    public String getMascara() {
        return FaceMask;
    } //Retorna si es que el individuo lleva mascarrilla en forma de string ("ON" o "OFF").

    // State Methods
    public void LookforPersonInf(ArrayList<Individuo> close_person, double d, double p0, double p1, double p2){
        for(int i=0; i< close_person.size(); i++){
            double D = Math.sqrt((x-(close_person.get(i).getX()))*(x-(close_person.get(i).getX())) + (y-(close_person.get(i).getY()))*(y-(close_person.get(i).getY())));
                if (D <= d && estadoSalud == "S" && close_person.get(i).getState() == "I"){
                    if (FaceMask == "ON" && close_person.get(i).getMascara()=="ON"){
                        double r = Math.random();
                        if (r <= p2) {
                                estadoSalud_tPlusDelta= "I";
                       }
                    }else if((FaceMask == "ON" && close_person.get(i).getMascara() == "OFF") ||
                        (FaceMask == "OFF" && close_person.get(i).getMascara() == "ON")){
                        double r = Math.random();
                        if (r <= p1) {
                            estadoSalud_tPlusDelta= "I";
                        }
                    }else if(FaceMask == "OFF" && close_person.get(i).getMascara() == "OFF"){
                        double r = Math.random();
                        if (r <= p0) {
                            estadoSalud_tPlusDelta= "I";
                        }
                    }
                }
            }//Calcula la distancia entre el individuo y cada uno de los otros que se encuentran en la comuna, y una vez que encuentra
            //uno lo suficientemente cerca, revisa  segun las probabilidades si es que se infectara.
    }// Metodo utilizado para escanear las personas cerca del individuo.
    public void startTimer(double delta_t, double tiempoInfeccion) {
        timerRecuperacion += delta_t;
        if (timerRecuperacion >= tiempoInfeccion) {
            estadoSalud_tPlusDelta = "R";
            timerRecuperacion = 0;
        }
    } //Este metodo que solo se ejecuta lo que esta adentro, si el individuo se infecta (estadoSalud == "I")
    public void computeAngle(){
        double r = Math.random(); // r entre 0 y 1
        double r2 = Math.random(); // r entre 0 y 1 para ver naturaleza del cambio de angulo
        if (r2 >= 0.5) {
            angle += r * deltaAngle; // cambia el angulo con delta positivo
        } else {
            angle -= r * deltaAngle; // cambia el angulo con delta negativo
        }
    } //Calculo del prox angulo en delta_t
    public void computeXY(double delta_t){
        double time;
        double time2;
        double angle2 = angle;

        if ((x + speed * delta_t * Math.cos(angle)) < 0) {
            time = x / (speed * (Math.abs(Math.cos(angle))));
            time2 = delta_t - time;
            x_tPlusDelta = x + speed * time * (Math.cos(angle));
            angle = (3 * Math.PI) - angle;
            x_tPlusDelta = x_tPlusDelta + speed * time2 * (Math.cos(angle));
        } else if ((x + speed * delta_t * Math.cos(angle)) > comuna.getWidth()) {
            time = (comuna.getWidth() - x) / (speed * (Math.abs(Math.cos(angle))));
            time2 = delta_t - time;
            x_tPlusDelta = x + speed * time * (Math.cos(angle));
            angle = (3 * Math.PI) - angle;
            x_tPlusDelta = x_tPlusDelta + speed * time2 * (Math.cos(angle));
        } else {
            x_tPlusDelta = x + speed * delta_t * Math.cos(angle);
        }

        if ((y + speed * delta_t * Math.sin(angle2)) < comuna.getHeight()
                && (y + speed * delta_t * Math.sin(angle2)) > 0) {
            y_tPlusDelta = y + speed * delta_t * Math.sin(angle2);

        } else if (y + speed * delta_t * Math.sin(angle2) > comuna.getHeight()) {

            time = (comuna.getHeight() - y) / (speed * (Math.abs(Math.sin(angle2))));
            time2 = delta_t - time;
            y_tPlusDelta = y + speed * time * Math.sin(angle2);
            angle2 = (2 * Math.PI) - angle2;
            y_tPlusDelta = y_tPlusDelta + (speed * time2 * (Math.sin(angle2)));
            angle = angle2;

        } else if (y + speed * delta_t * Math.sin(angle2) < 0) {
            time = y / (speed * (Math.abs(Math.sin(angle2))));
            time2 = delta_t - time;
            y_tPlusDelta = y + speed * time * Math.sin(angle2);
            angle2 = (2 * Math.PI) - angle2;
            y_tPlusDelta = y_tPlusDelta + (speed * time2 * ((Math.sin(angle2))));
            angle = angle2;
        }
    } //Calculo de x_tPlusDelta y y_tPlusDelta
    public void computeNextState(double delta_t, double d, double p0, double tiempoInfeccion, double p1, double p2) {
        if (estadoSalud == "S") {
            LookforPersonInf(comuna.getPeople(), d, p0, p1, p2);
        } else if (estadoSalud == "I") {
            startTimer(delta_t, tiempoInfeccion);
        }
        computeAngle();
        computeXY(delta_t);
        // Aumenta x e y tomando el angulo nuevo y el tiempo de mov. si la persona sale de la comuna rebota.
    } //El resto de los metodos de Compute se encuentran dentro de este.

    //Set Methods
    public void setMask(){
        FaceMask = "ON";
    }

    //Update Methods - Utilizados para actualizar las coords del individuo y su estado de salud.
    public void updateState() {
        x = x_tPlusDelta;
        y = y_tPlusDelta;
        estadoSalud = estadoSalud_tPlusDelta;
    } //Va actualizando posiciones y estados de salud del individuo.
    public void updateStateRandom(double x, double y) {
        this.x = x;
        this.y = y;
        estadoSalud = estadoSalud_tPlusDelta;
    } //Solo es utilizado cuando la comuna busca colocar al individuo en un lugar random de su territorio.
}

